let firstLetterCapital = (input) => {
  let inputAr = input.split("");

  let outputAr = inputAr.map((value, i) => {
    if (i === 0) {
      return value.toUpperCase();
    } else {
      return value.toLowerCase();
    }
  });

  let output = outputAr.join("");

  return output;
};

let eachWordCapital = (input) => {
  let inputAr = input.split(" ");

  let inputAr1 = inputAr.map((value, i) => {
    return firstLetterCapital(value);
  });

  let output = inputAr1.join(" ");

  return output;
};

console.log(eachWordCapital("hello my name is nitan thapa"));

// eachWordCapital("my name is") => "My Name Is"
