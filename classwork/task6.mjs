// price greater than  700 ["earphone", "battery"]
let products = [
  { name: "earphone", price: 1000 },
  { name: "battery", price: 2000 },
  { name: "charger", price: 500 },
];

let productsG700 = products.filter((value, i) => {
  if (value.price > 700) {
    return true;
  } else {
    return false;
  }
});

let desiredOutput = productsG700.map((value, i) => {
  return value.name;
});

console.log(desiredOutput);
