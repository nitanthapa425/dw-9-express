//make a function that takes any number of inputs and returns the sum of all those inputs

// sum(1,2)
// sum(1,2,3)
// sum(1,2,3,4)

let sum = (...inputs) => {

  let output = inputs.reduce((pre, cur) => {
    return pre + cur;
  }, 0);

  return output;
};

let _sum = sum(1, 2, 3, 4, 1, 2, [1, 2]);
console.log(_sum);
