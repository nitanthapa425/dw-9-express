//primitive
// string, number, boolean, undefined, null

//non primitive
// object, array

//===

// let a = 1;
// let b = a;//1
// a =5
// console.log(a);//5
// console.log(b);//1

// let ar1 = [1, 2];
// let ar2 = ar1;
// ar2.push(3);
// console.log(ar1); //[1,2]   => [1,2,3]
// console.log(ar2); //[1,2,3]  => [1,2,3]
