// let ar = ["nitan",29,false]

// array is made by the combination of value
// object is made by the combination of key value pair
// name => key
// "nitan"=> value
// name:"nitan" => property

let obj = {
  isMarried: false,
  name: "nitan",
  age: 29,
};

//get whole object
console.log(obj);

//get specific element
console.log(obj.name);
console.log(obj.age);
console.log(obj.isMarried);

//change specific element
obj.name = "ram";
console.log(obj);

//delete specific element

delete obj.age;
console.log(obj);
