let obj = {
  name: "nitan",
  age: 29,
  isMarried: false,
};

//["name","age","isMarried"]  =>key
//["nitan",29, false] =>value
///[["name","nitan"],["age",29],["isMarried",false]] =>property

let keyArr = Object.keys(obj);
let valueArr = Object.values(obj);
let propArr = Object.entries(obj);
