//we can not convert all array to obj
// can only convert if we have array like [ ["name","nitan"], ["age",29]]
// here we have array of array and inner array has length of 2

let ar = [
  ["name", "nitan"],
  ["age", 29],
];

let info = Object.fromEntries(ar);
console.log(info);
