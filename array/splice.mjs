//splice

let inputAr = ["a", "b", "c", "d", "e"];
//              0 ,  1 ,  2 ,  3 ,  4

// let outputAr1 = inputAr.splice(1, 2, "ram", "hari"); // it returns the deleted items
//1 is starting index
//2 no of element to delete

// ram , hari shyam, true are the value to be added from starting index

//[a,ram,hari, shyam, true,d,e]

let outputAr1 = inputAr.splice(1); // if no of item to be deleted is missing then it will  removed all item to the end
console.log(inputAr);
console.log(outputAr1);
