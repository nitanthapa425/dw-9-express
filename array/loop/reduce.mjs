let ar1 = [1, 2, 3, 4, 5];

//                       10  ,  5
let value = ar1.reduce((pre, cur) => {
  return pre + cur;
}, 0); //1//3//6//10//15

console.log(value);
