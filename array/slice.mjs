let ar = ["a", "b", "c", "d", "e"];
//          0 , 1 ,   2 ,   3,  4

//
// let ar1 = ar.slice(1, 5);// 1 is starting index and 5 is end index (end index must be always greater than 1)
// console.log(ar1);

// if end index is missing slice method will slice to the end
let ar2 = ar.slice(2);

console.log(ar2);

console.log(ar);
