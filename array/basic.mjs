let ar = ["nitan",29,false]
//          0     , 1 , 2

//array can store multiple value of different data type


//get whole array
console.log(ar)

//get specific element of array
console.log(ar[0])
console.log(ar[1])
console.log(ar[2])


//change specific element of array
ar[1]=30
ar[2]= true

console.log(ar[0])
console.log(ar[1])
console.log(ar[2])


//delete specific element of array

delete ar[1]
console.log(ar)
