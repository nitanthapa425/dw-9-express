// [1,2,["a"]] [1,2,["a"],["a"],1]
//  ...   // m => stringify
// ["1","2","["a"]","["a"]","1"]
//  ...   // using set
//  ["1","2","["a"]"]
// ......// map  ..parsegit remote add origin https://gitlab.com/nitanthapa425/dw-9-express.gitgit remote add origin https://gitlab.com/nitanthapa425/dw-9-express.git

// =>[1,2,["a"]]

let input = [1, 2, ["a"], ["a"], 1];

let inputStr = input.map((value, i) => {
  return JSON.stringify(value);
});
// console.log(inputStr);

let uniqueValueStr = [...new Set(inputStr)];
// console.log(uniqueValueStr)

let output = uniqueValueStr.map((value, i) => {
  return JSON.parse(value);
});
console.log(output);
