let list = [1, 2, 3];
console.log(list); //[1,2,3]

let listStr = JSON.stringify(list);

console.log(listStr); //'[1,2,3]'

let listStrParse = JSON.parse(listStr); //[1,2,3]
console.log(listStrParse);
